#!/usr/bin/env node

const fs = require('fs'),
      path = require('path'),
      xml = require('xml-js'),
      fetch = require('./lib/fetchassets'),
      status = require('./lib/status'),
      doc = require('./lib/doc'),
      merge = require('./lib/merge'),
      exp = require('./lib/export'),
      add = require('./lib/add'),
      group = require('./lib/group'),
      setup = require('./lib/setup'),
      pack = require('./lib/pack'),
      serve = require('./lib/serve.js'),
      server = require('./lib/server.js')
      version = require('./package.json').version;

let [a0, a1, ...args] = process.argv;
let options = {config: process.cwd() + '/config.json'};
while (String(args[0]).startsWith('-')) {
  switch(args[0]) {
    case '-c': /* other config file */
      args.shift();
      if (path.isAbsolute(args[0])) {
        options.config = path.normalize(args.shift());
      } else {
        options.config = path.normalize(process.cwd() + '/' + args.shift());
      }
      break;
    case '-t': /* another target */
      args.shift();
      options.target = path.normalize(args.shift());
      console.log('TARGET is', options.target);
      break;
  }
}
let target = options.target || 'default';
options.basedir = path.dirname(options.config);
args.unshift(a1);
args.unshift(a0);

if (args[2] === 'version') {
    console.log(version);
    process.exit();
}
if (args[2] === 'help') {
  console.log(`
Usage:

csfetch [options] [command]

Options
  -c <config.json>  default is config.json in current directory

Commands
csfetch add <asset-id or resource-key or uuid> [group]
csfetch merge config-json-file
csfetch status
csfetch doc [my-own-file-name] [group]
csfetch version
csfetch export [group]
csfetch group [name] [export=xxx] [subpath=xxx]
csfetch setup
csfetch pack [group]
csfetch serve
csfetch server <options>
csfetch xsl
csfetch help
`);
  process.exit();
}

if (! fs.existsSync(options.config)) {
  if (args[2] === 'setup') {
    setup.config(null, {config: process.cwd() + '/config.json', setup:{}});
  } else {
    if (args[2] === 'xsl') {
      fs.copyFileSync(__dirname + '/create-config.xsl', process.cwd() + '/create-config.xsl');
      console.log('Use the file "create-config.xsl" in a Censhare server to generate a config file.')
    } else {
      console.log('Need a "config.json" file. Run "csfetch setup" to create one, or "csfetch xsl" to get a xsl file that can be used on a Censhare server.');
    // console.log('Need a "config.json" file. Use the file "create-config.xsl" in a Censhare server to create it.');
    // fs.copyFileSync(__dirname + '/create-config.xsl', process.cwd() + '/create-config.xsl');
    }
  }
  //process.exit();
//}
} else {

  var config = JSON.parse(fs.readFileSync(options.config));

  /* we are running in private spaces with private certificates, and don't want warnings from node */
  let ws = process.stdout.write;
  process.stderr.write = null;
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  process.stdout.write = ws;

  function toBase64 (str) {
    return Buffer.from(str || '', 'utf8').toString('base64')
  }

  function authHash (username, password) {
    var header = username + ':' + (password || '')
    let authHeader = 'Basic ' + toBase64(header)
    return authHeader;
  }

  if (config.setup) {
    config.auth = authHash(config.setup.username, config.setup.password);
    config._temp || (config._temp = {default:{}});
    config._temp.default.auth = authHash(config.setup.username, config.setup.password);
    if (config.target) {
      Object.keys(config.target).forEach(t => {
        config._temp[t] || (config._temp[t] = {});
        config._temp[t].auth = authHash(config.target[t].username, config.target[t].password);
      })
    }
  }

  if (args[2] === 'status') {
    status.check(null, config, options);
  } else if (args[2] === 'update') {
    status.check(null, config, (files) => {
      console.log('Downloading ' + Object.keys(files).length + ' assets.');
      fetch.fromServer({
        setup: config.setup,
        files: files,
        auth: authHash(config.setup.username, config.setup.password)
      });
    });
  } else if (args[2] === 'merge') {
    merge.configs(args[3], options);
  /*} else if (args[2] === 'config') {
    let conf = require(process.cwd() + '/' + args[3]);
    conf.auth = config.auth;
    fetch.fromServer(conf) */
  } else if (args[2] === 'doc') {
    args[4] && (options.group = args[4]);
    doc.createDocFile(args[3] || 'documentation.md', config, options);
  } else if (args[2] === 'export') {
    args[3] && (options.group = args[3]);
    exp.prepare(config, options, () => {console.log('--- exported ---')});
  } else if (args[2] === 'add') {
    options.ids = args.slice(3);
    add.addOne(config, options);
  } else if (args[2] === 'setup') {
    setup.config(config, options);
  } else if (args[2] === 'group') {
    options.group = args[3];
    options.groupargs = args.slice(3);
    group.groups(config, options);
  } else if (args[2] === 'pack') {
    options.group = args[3];
    pack.pack(config, options);
  } else if (args[2] === 'serve') {
    serve.serve(config /*port*/);
  } else if (args[2] === 'server') {
    server.connect(config, options);
  } else {
    options.group = args[3] ? undefined : args[2];
    fetch.fromServer(config, null, options);
  }
}
