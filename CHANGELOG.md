# Changelog

## Version 0.22.1:

Removed asset-rel-feature "module-rel" in export.

## Version 0.22.0:

Extara config entries, pack path, pretty print for xml, and cleanup for asset-rel-features.

## Version 0.21.2:

Export in scripting API and remove possible module.source feature.

## Version 0.21.1:

GUI closes the window on exit.

## Version 0.21.0:

Mocha testing. Started to build a scripting API for Nodejs.

## Version 0.20.4:

Corrected status command.

## Version 0.20.3:

Supress warning.

## Version 0.20.2;

Config file in option to the command.

## Version 0.17.0

Module _request_ replaced with _node-fetch_.

## Version 0.16.2

Exporting extra storage items.

## Version 0.16.0

Can export other storage items than "master".

## Version 0.15.2

Adjusted doc generation to exclude not exported items.

## Version 0.13.8

Depricated resource key as key. Added it as property instead, if it exsists. Adding group to excisting fix.

## Version 0.13.7

Changed when add group to existing asset.

## Version 0.13.6

Moved out code to new modules saver.js and mangle.js. Filter out messages.

## Version 0.9.9

An entry can belong to several groups. 

## Version 0.8.4

Moved to gitlab.
