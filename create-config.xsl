<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:cs="http://www.censhare.com/xml/3.0.0/xpath-functions"
	xmlns:corpus="http://www.censhare.com/xml/3.0.0/corpus"
	xmlns:my="http://ns.censhare.de/my"
	xmlns:func="http://ns.censhare.de/functions"
	exclude-result-prefixes="#all">
	
	<xsl:output method="text" indent="yes"/>
	
	<!-- Choose if you want to fetch all custom assets or selected ones            -->
	<!--  mode= 'all' - fetch all assets with $customPrefix and ctrl: prefix       -->
	<!--  mode = 'selected' - fetch selected assets only and it's assigned chidren --> 
	<xsl:variable name="mode" select="'all'"/>
	<xsl:variable name="customPrefix" select="'vt'"/>
	
	<xsl:variable name="query-common-conditions">
		<not>
			<or>
				<condition name="censhare:asset-flag" op="=" value="is-template"/>
				<condition name="censhare:asset-flag" op="=" value="copy-template"/>
			</or>
		</not>
	</xsl:variable>

	
	<xsl:template match="/">
		
		<xsl:variable name="allCustomAssetsQuery">    		
			<query type="asset">
				<or>
					<condition name="censhare:resource-key" value="{concat($customPrefix, ':*')}" op="like" /> 
					<condition name="censhare:resource-key" value="ctrl:*" op="like" /> 
				</or> 
			</query>
		</xsl:variable>
		<xsl:variable name="allCustomAssets" select="if ($mode='all') then cs:asset($allCustomAssetsQuery) else()"/>

		<xsl:variable name="xml" as="element(map)">
			<map>
				<map key="setup">
					<string key="username">
						<xsl:value-of select="cs:master-data('party')[@id=system-property('censhare:party-id')]/@login"/>
					</string>
					<string key="password"/>
					<string key="server">
						<xsl:value-of select="concat(system-property('censhare:host-name'), ':9443')"/>
					</string>
					<string key="service">
						<xsl:value-of select="'/ws/rest/service'"/>
					</string>
				</map>
				<map key="files">
					<xsl:choose>
						<xsl:when test="$mode='all'">
							<xsl:apply-templates mode="path-find" select="$allCustomAssets">
								<xsl:with-param name="path" select="'storage'"/>
							</xsl:apply-templates>
						</xsl:when>
						<xsl:when test="$mode='selected'">
							<xsl:apply-templates mode="path-find">
								<xsl:with-param name="path" select="'storage'"/>
							</xsl:apply-templates>
						</xsl:when>
					</xsl:choose>
				</map>
			</map>
		</xsl:variable>
		<xsl:apply-templates select="$xml"/>
	</xsl:template>
	
	<xsl:template match="asset" mode="path-find">
		<xsl:param name="path"/>
		
		<xsl:variable name="current-asset" select="." />
		<xsl:if test="$mode='selected' and ./child_asset_rel[@key='user.']">
			<xsl:variable name="child" select="(distinct-values(cs:get-asset(./@id)/child_asset_rel/@child_asset))/cs:asset()"/>
			<xsl:apply-templates select="$child" mode="path-find">
				<xsl:with-param name="path" select="concat($path, '/', my:normalize(./@name))"/>
			</xsl:apply-templates>
		</xsl:if>
		<xsl:variable name="resource-key" select="asset_feature[@feature='censhare:resource-key']/@value_string"/>
		<xsl:variable name="key" select="@id"/>
		<xsl:variable name="a-name" select="if(string-length($key) &gt; 0) then ./asset_feature[@feature='censhare:uuid']/@value_string else ./asset_feature[@feature='censhare:uuid']/@value_string"/>
		<map key="{$key}">
			<xsl:if test="storage_item[@key='master']">
				<xsl:variable name="suffix" select="tokenize(storage_item[@key='master']/@relpath, '\.')[last()]"/>
				<string key="path">
					<xsl:value-of select="concat($path, '/', my:normalize(./@name), '.', ./@id, '.', $suffix)"/>
				</string>
			</xsl:if>
			<string key="metadata">
				<xsl:value-of select="concat('assets/', my:normalize($a-name), '.asset.xml')"/>
			</string>
			<xsl:if test="string-length($resource-key) &gt; 0">
				<string key="resourcekey">
					<xsl:value-of select="$resource-key" />
				</string>
			</xsl:if>
			<xsl:variable name="folder" select="$asset-cache/cache/entry[@asset_id = $current-asset/@id]" />
			<xsl:if test="not(string-length($folder) = 0)">
				<string key="export">
					<xsl:value-of select="$folder" />
				</string>
			</xsl:if>
		</map>
	</xsl:template>
	
	<xsl:template match="map">
		<xsl:if test="@key">
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@key"/>
			<xsl:text>": </xsl:text>
		</xsl:if>
		<xsl:text>{</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>}</xsl:text>
		<xsl:if test="last() != position()">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="array">
		<xsl:if test="@key">
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@key"/>
			<xsl:text>": </xsl:text>
		</xsl:if>
		<xsl:text>[</xsl:text>
		<xsl:apply-templates/>
		<xsl:text>]</xsl:text>
		<xsl:if test="last() != position()">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="string">
		<xsl:if test="@key">
			<xsl:text>"</xsl:text>
			<xsl:value-of select="@key"/>
			<xsl:text>": </xsl:text>
		</xsl:if>
		<xsl:text>"</xsl:text>
		<xsl:value-of select="."/>
		<xsl:text>"</xsl:text>
		<xsl:if test="last() != position()">
			<xsl:text>,</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<xsl:function name="my:normalize">
		<xsl:param name="name"/>
		<xsl:value-of select="replace(replace(replace(replace($name, '/', '_'), ' ', '_'), ':', '-'), '\.', '-')"/>
	</xsl:function>

	<xsl:template name="sort-resource-replace-variants">
		<xsl:param name="allAssets" />
		<xsl:param name="level" as="xs:int" />
		
		<xsl:variable name="items" select="$allAssets[not(child_asset_rel and child_asset_rel/@key='variant.resource.replace.') or not(child_asset_rel/@child_asset = $allAssets/@id and child_asset_rel/@key='variant.resource.replace.')]" />
		<xsl:variable name="filtered" as="element(asset)*" select="$allAssets[not(@id = $items/@id)]" />
		<xsl:if test="count($filtered) &gt; 0">
			<xsl:call-template name="sort-resource-replace-variants">
				<xsl:with-param name="allAssets" select="$filtered" />
				<xsl:with-param name="level" select="$level + 1" />
			</xsl:call-template>
		</xsl:if>
		<xsl:for-each select="$items">
			<asset>
				<xsl:copy-of select="@*" />
				<xsl:attribute name="export_level" select="$level" />
				<xsl:copy-of select="*" />
			</asset>
		</xsl:for-each>
	</xsl:template>
	
	<!-- cache for pairs: asset-id => directory -->
	<xsl:variable name="asset-cache">
		<cache>
			<xsl:for-each select="$allQueries/item">
				<xsl:variable name="item" select="." />
				<xsl:variable name="folder" select="$item/folder" />
				<xsl:variable name="query">
					<query>
						<xsl:copy-of select="$item/query/@*" />
						<xsl:copy-of select="$item/query/*" />
						<xsl:if test="not($item/@ignore_common_conditions)">
							<xsl:copy-of select="$query-common-conditions" />
						</xsl:if>
					</query>
				</xsl:variable>
				<xsl:variable name="assets" select="cs:asset($query)" />
				<xsl:choose>
					<xsl:when test="$assets[starts-with(@type, 'module.workspace.') and (parent_asset_rel/@key='variant.resource.replace.' or child_asset_rel/@key='variant.resource.replace.')]">
						<xsl:variable name="sorted">
							<xsl:call-template name="sort-resource-replace-variants">
								<xsl:with-param name="allAssets" select="$assets" />
								<xsl:with-param name="level" select="1" />
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="max" select="($sorted/asset[1])/@export_level" />
						<xsl:for-each select="$sorted/asset">
							<xsl:variable name="directory" select="concat('000', xs:long($max - @export_level))" />
							<entry asset_id="{@id}"><xsl:value-of select="concat($folder, '/', substring($directory, string-length($directory)-2))" /></entry>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each select="$assets">
							<entry asset_id="{@id}"><xsl:value-of select="$folder" /></entry>
						</xsl:for-each>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</cache>
	</xsl:variable>
	
	<!-- every export folder should have different query below -->
	<xsl:variable name="allQueries">
		<item>
			<folder>000_transformations</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.transformation."/>
			</query>
		</item>
		<item>
			<folder>005_images</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="picture."/>
			</query>
		</item>
		<item>
			<folder>010_localizations</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.localization."/>
			</query>
		</item>
		<item>
			<folder>020_macros</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.macro."/>
			</query>
		</item>	
		<item>
			<folder>030_searches/01_search-transformations</folder>
			<query type="asset">
				<or>
					<condition name="censhare:asset.type" op="=" value="module.search."/>
					<condition name="censhare:asset.type" op="=" value="module.search.transformation."/>
				</or>
			</query>
		</item>
		<item>
			<folder>030_searches/02_search-related</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.search.related."/>
			</query>
		</item>
		<item>
			<folder>040_configurations/01_asset-relations</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.config.asset-relation."/>
			</query>
		</item>
		<item>
			<folder>040_configurations/02_features</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.feature."/>
			</query>
		</item>
		<item>
			<folder>040_configurations/03_value-lists</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.value-list."/>
			</query>
		</item>
		<item>
			<folder>040_configurations/04_configs</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.config."/>
			</query>
		</item>
		<item>
			<folder>040_configurations/05_categories_topics</folder>
			<query type="asset">
				<or>
					<condition name="censhare:asset.type" op="=" value="category."/>
					<condition name="censhare:asset.type" op="=" value="category.topic."/>
				</or>
			</query>
		</item>
		<item>
			<folder>040_configurations/06_scripts</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.script.indesign."/>
			</query>
		</item>
		<item>
			<folder>050_workspace-views/01_snippets</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.view.snippet."/>
			</query>
		</item>
		<item>
			<folder>050_workspace-views/02_slots</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.view.slot."/>
			</query>
		</item>
		<item>
			<folder>050_workspace-views/03_views</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.view."/>
			</query>
		</item>
		<item>
			<folder>060_tables/01_columns</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.table.column."/>
			</query>
		</item>
		<item>
			<folder>060_tables/02_tables</folder>
			<query type="asset">				
				<condition name="censhare:asset.type" op="=" value="module.table."/>
			</query>
		</item>	
		<item>
			<folder>070_output-tables/generators</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.output-table.generator."/>
			</query>
		</item>
		<item>
			<folder>080_categories</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.category."/>
			</query>
		</item>
		<item>
			<folder>090_channels</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="channel."/>
			</query>
		</item>
		<item>
			<folder>100_branding</folder>
			<query type="asset">
				<or>
					<condition name="censhare:asset.type" op="=" value="module.branding."/>
					<xsl:for-each select="cs:asset()[@censhare:asset.type='module.branding.']">
						<condition name="censhare:asset.id" op="=" value="{./asset_feature[@feature='censhare:login.logo']/@value_asset_id}"/>
						<condition name="censhare:asset.id" op="=" value="{./asset_feature[@feature='censhare:login.web-page']/@value_asset_id}"/>
						<condition name="censhare:asset.id" op="=" value="{./asset_feature[@feature='censhare:top-navigation.logo']/@value_asset_id}"/>
					</xsl:for-each>
				</or>
			</query>
		</item>
		<item>
			<folder>110_completeness-checks</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.completeness-check."/>
			</query>
		</item>
		<item>
			<folder>120_content-editor</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.content.*"/>
			</query>
		</item>
		<item>
			<folder>125_dialogs/01_scripts</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.script."/>
			</query>
		</item>
		<item>
			<folder>125_dialogs/02_dialogs</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.dialog."/>
			</query>
		</item>
		<item>
			<folder>130_dialog-groups</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.dialog.group."/>
			</query>
		</item>
		<item>
			<folder>140_inheritance-rules</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.feature-inheritance-rule."/>
			</query>
		</item>
		<item>
			<folder>150_layout-editor</folder>
			<query type="asset">	
				<or>
					<condition name="censhare:asset.type" op="=" value="module.layout-box."/>
					<condition name="censhare:asset.type" op="=" value="module.layout-box-tag."/>
					<condition name="censhare:asset.type" op="=" value="module.layout-group."/>
				</or>
			</query>
		</item>
		<item>
			<folder>160_lookup-rules</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.feature-lookup-rule."/>	
			</query>
		</item>
		<item>
			<folder>170_presets</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.preset."/>
			</query>
		</item>
		<item>
			<folder>200_workspace/10_widget-types</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.widget."/>	
			</query>
		</item>
		<item>
			<folder>200_workspace/20_widget-config</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.widget.config."/>	
			</query>
		</item>
		<item>
			<folder>200_workspace/30_containers</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.container."/>	
			</query>
		</item>
		<item>
			<folder>200_workspace/40_pages</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.page."/>	
			</query>
		</item>
		<item>
			<folder>200_workspace/50_navigation/02_navigation-groups</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.navigation-left.group."/>	
			</query>
		</item>
		<item>
			<folder>200_workspace/50_navigation/01_navigation-items</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace.navigation-left.item."/>
			</query>
		</item>
		<item>
			<folder>200_workspace/60_workspace</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="module.workspace."/>
			</query>
		</item>
		<item>
			<folder>210_server-actions/01_action-groups</folder>
			<query type="asset">	
				<condition name="censhare:asset.type" op="=" value="module.action.group."/>
			</query>
		</item>
		<item>
			<folder>210_server-actions/03_actions</folder>
			<query type="asset">	
				<condition name="censhare:asset.type" op="=" value="module.action.server."/>
			</query>
		</item>
		<item ignore_common_conditions="true">
			<folder>300_asset-templates</folder>
			<query type="asset">
				<or>
					<condition name="censhare:asset-flag" op="=" value="is-template"/>
					<condition name="censhare:asset-flag" op="=" value="copy-template"/>
				</or>
			</query>
		</item>
		<item>
			<folder>600_project-specific</folder>
			<query type="asset">
				<or>
					<condition name="censhare:resource-key" op="=" value="vt:pim-import-config"/>
					<condition name="censhare:resource-key" op="=" value="vt:connection-config"/>
				</or>
			</query>
		</item>
		<item>
			<folder>700_directory-structure</folder>
			<query type="asset">
				<condition name="censhare:asset.type" op="=" value="group."/>
			</query>
		</item>
	</xsl:variable>
	
</xsl:stylesheet>
