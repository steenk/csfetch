
const fs = require('fs'),
      xml = require('xml-js'),
      kvp = require('key-value-pointer');

var t = 0;

function createDocFile (filename, config, options) {
	filename = kvp.basename(filename, '.md') + '.md';
	let files = Object.keys(config.files);
	let sorted = {};
	let group = options.group;
	let info = {
		title: config.title || 'Documentation',
		description: config.description || ''
	}
	files.forEach((key, i) => {
		if (group && group !== 'default' && [].concat(config.files[key].group).indexOf(group) === -1) {
    		return;
    	}
		let f = config.files[key].metadata;
		let show = !(config.files[key].export && config.files[key].export.startsWith('_no'));
		fs.readFile(f, (err, data) => {
			if (err) throw err;
			let file = kvp.basename(config.files[key].path||'');
			let dir = kvp.dirname(config.files[key].path||'').replace(/^\w+\/\w+\//,'');
			let comment = config.files[key].comment;
			let card = config.files[key].card;
			let cardurl = config.setup.issue_tracker && (config.setup.issue_tracker.base_url + card);
			let metadata = JSON.parse(xml.xml2json(data, {compact: true}));
			let storages = kvp(metadata).query(function (node) {
				if (node.key === 'storage_item') {
					return true
				}
			})
			let master = kvp(storages).query((node) => {if (node.key === 'key' && node.value === 'master') {
					return kvp(storages).select(kvp.dirname(kvp.dirname(node.pointer)))._attributes;
				}
			});
			let rk = kvp(metadata).query(function (node) {
				if (node.key === 'feature' && node.value === 'censhare:resource-key') {
					return (this.select(kvp.dirname(node.pointer))).value_asset_key;
				}
			});
			let uuid = kvp(metadata).query(function (node) {
				if (node.key === 'feature' && node.value === 'censhare:uuid') {
					return (this.select(kvp.dirname(node.pointer))).value_string;
				}
			});
			var item = {
				name: metadata.asset._attributes.name,
				assetType: metadata.asset._attributes.type,
				uuid: uuid,
				creationDate: new Date(metadata.asset._attributes.creation_date).toUTCString(),
				resourceKey: typeof rk === 'string' ? rk : '',
				fileName: file,
				dirName: dir,
				mimeType: master.mimetype,
				fileSize: Math.round((master.filelength)||0 / 1024) + 'kB',
				comment: comment,
				card: card,
				cardurl: cardurl
			};
			show && (sorted[item.assetType] || (sorted[item.assetType] = [])) && sorted[item.assetType].push(item);

			if ((files.length - 1) === i) {
				sortAndWrite(filename, sorted, info);
			}
		})
	});
}

function sortAndWrite (filename, data, info) {
	var content = '# ' + info.title + '\n\n';

	if (info.description) {
		content += info.description + '\n\n';
	}
	
	Object.keys(data).forEach((type) => {
		content += '## Assets of type ' + type + '\n';
		content += 'Number of assets of type "' + type + '" are ' + data[type].length + '.\n';
		data[type].forEach((item) => {
			content += '### ' + item.name + '\n\n';
			if (item.card) {
				content += '\n';
				if (item.cardurl) {
					content += '[' + item.card + '](' + item.cardurl + ') ';
				} else {
					content += '**' + item.card + '** ';
				}
				content += '\n\n';
			}
			if (item.comment) {
				content += item.comment + '\n\n';
			}
			content += '    Asset Type: ' + item.assetType + '\n';
			content += '    UUID: ' + item.uuid + '\n';
			content += '    Created: ' + item.creationDate + '\n';
			if (item.resourceKey) {
 				content += '    Resource Key: ' + item.resourceKey + '\n';
 			}
 			if (item.fileName) {
 				content += '    Filename: ' + item.fileName + '\n';
 				content += '    Location: ' + item.dirName + '\n';
 				content += '    MIME-Type: ' + item.mimeType + '\n';
 				content += '    File Size: ' + item.fileSize + '\n';
 			}
 			content += '\n';
		})
	});

	fs.writeFileSync(filename, content);

}

module.exports = {createDocFile}