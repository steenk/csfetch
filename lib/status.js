const fs = require('fs'),
      xml = require('xml-js'),
      fetch = require('node-fetch'),
      https = require('https'),
      path = require('path');

const agent = new https.Agent({
    rejectUnauthorized: false
});

function check (ws, config, options, cb) {
	let basedir = options.basedir || '.';
	ws = ws || process.stdout;
	function log (data) {
		ws.write(data);
		ws.write('\n');
	}
	//let config = require(process.cwd() + '/config.json');
	let setup = config.setup;
	let headers = {Authorization: config._temp[options.target||'default'].auth};
	let filesInConfig = [];
	let changed = {};
	Object.keys(config.files).forEach((key) => {
		let data = JSON.parse(JSON.stringify(config.files[key]));
		data.key = key;
		filesInConfig.push(data);
	});
	if (! fs.existsSync(path.join(basedir, 'assets'))) {
		log('No "config.json".');
		return;
	}

	fs.readdir(path.join(basedir, 'assets'), (err, files) => {
		if (err) throw err;
		let checkedFiles = {};
		let cnt = files.length;
		files.forEach((f) => {
			checkedFiles['assets/' + f] = true;
			fs.readFile(path.join(basedir, 'assets', f), (err, data) => {
				if (err) throw err;
				let storedAsset = JSON.parse(xml.xml2json(data, {compact: true}));
				let id = storedAsset.asset._attributes.id;
				let url = `https://${setup.server}${setup.service}/assets/asset/id/${id}`;
				fetch(url, {headers, agent})
                .then(res => res.buffer())
                .then(body => {
					let serverAsset = JSON.parse(xml.xml2json(body, {compact: true}));
					if (!serverAsset.asset) {
						log('Error on id ' + id + ', ' + f);
						process.exit(1);
					}
					let modDate = serverAsset.asset._attributes.modified_date;
					let name = serverAsset.asset._attributes.name;
					if (storedAsset.asset._attributes.modified_date !== modDate) {
						log('Asset ' + name + ' (' + id + ') has changed on the server.');
						changed['assets/' + f] = true;
					}
					if (serverAsset.asset._attributes.state === '1') {
						log('Asset ' + name + ' (' + id + ') is checked out by ' + serverAsset.asset._attributes.checked_out_by + '.');
					}
					cnt--;
					if (cnt <= 0) {
						collect()
					}
				})
				.catch(err => {console.log(err)});
			})
		});
		function collect () {
			let forUpdate = {};
			filesInConfig.forEach((f) => {
				if ((!checkedFiles[f.metadata]) || changed[f.metadata]) {
					if (!checkedFiles[f.metadata]) {
						log('Asset with key "' + f.key + '" is not downloaded yet.') 
					}
					forUpdate[f.key] = {
						metadata: f.metadata
					};
					if (f.path) {
						forUpdate.path = f.path;
					}
	 			}
	 		});
	 		cb && cb(forUpdate);
	 	}
	});
}

module.exports = {check}