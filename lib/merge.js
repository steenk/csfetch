const fs = require('fs');

function configs (conf, options) {

	if (! fs.existsSync(process.cwd() + '/' + conf)) {
  		console.log('Need a file to merge with the "config.json" file.');
 		process.exit();
	}
	fs.readFile(process.cwd() + '/' + conf, (err, data) => {
		if (err) throw err;
		let config = JSON.parse(fs.readFileSync(options.config));
		let newConfig = JSON.parse(data);
		Object.keys(newConfig.files).forEach((key) => {
			if (! config.files[key]) {
				config.files[key] = newConfig.files[key];
			}
		});
		delete config.auth;
		fs.writeFileSync(options.config, JSON.stringify(config, null, ' '));
	});
}

module.exports = {configs}