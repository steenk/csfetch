const fs = require('fs'),
      path = require('path'),
      expo = require('./export'),
      tar = require('tar'),
      checkDir = require('./checkDir').checkDirSync;

function pack (config, options, cb) {

  let basedir = options.basedir || '.';
	let group = options.group;
  let packdir = config.setup.packdir || 'censhare/censhare-Custom/censhare-Server/app/';
  let builddir = packdir.split(path.sep)[0];

	if (!group || !config.groups[group]) {
    group = 'default';
		console.log('Will use the default group.');
	}

  expo.prepare(config, options, function () {

  	let files = [];
  	let exp = {};

  	let keys = Object.keys(config.files);
    let groupexportpath = (config.groups && config.groups[group]) ? config.groups[group].export||'' : '';
    checkDir(path.join(basedir, 'packages', packdir, groupexportpath, '*'));
    	keys.forEach((key) => {
    		if (group && group !== 'default' && [].concat(config.files[key].group).indexOf(group) === -1) {
        	return;
        };
        exp[key] = config.files[key];
        if (! exp[key].export) {
          exp[key].export = '';
    		  exp[key].pack = path.join(packdir, groupexportpath);
        } else {
          exp[key].pack = path.join(packdir, exp[key].export);
        }
        checkDir(path.join(basedir, 'packages', exp[key].pack, '*'));
      });

      Object.keys(exp).forEach((key) => {
          if (exp[key].export && exp[key].export.toLowerCase().startsWith('_no')) return;
        	let asset = path.basename(exp[key].metadata);
        	let uid = asset.substring(0, 8);
          const rename = !!exp[key].export_name;
          const asset_name = rename ? exp[key].export_name.substring(0, exp[key].export_name.lastIndexOf('.')) + '.asset.xml' : asset;
        	let file = rename ? exp[key].export_name : uid + '_' + path.basename(exp[key].path||'');
          if (fs.existsSync(path.join(basedir, 'export', (exp[key].export || groupexportpath), file))) {
              fs.copyFileSync(path.join(basedir, 'export', (exp[key].export || groupexportpath), file),
                   path.join(basedir, 'packages', exp[key].pack, file));
              files.push(exp[key].pack + '/' + file);
          }
          fs.copyFileSync(path.join(basedir, 'export', (exp[key].export || groupexportpath), asset_name),
               path.join(basedir, 'packages', exp[key].pack, asset_name));
         	files.push(exp[key].pack + '/' + asset_name);
      });
      let tarfile = path.join(basedir, 'packages', 'package_' + (new Date().toISOString().substring(0, 10)) + '.tgz');
    	tar.create({
        	gzip: true,
        	file: tarfile,
        	C: path.join(basedir, 'packages')
      	}, files)
      .then(_ => {
      	console.log('"' + tarfile + '" has been created.');
        if (fs.existsSync(path.join(basedir, 'packages', builddir))) {
          fs.rm(path.join(basedir, '/packages', builddir), {recursive: true}, (err) => {
            if (err) throw err; 
            cb && cb();
          });
        } else {
          cb && cb();
        }
      })
      .catch(console.error); 
    }
  );
}

module.exports = { pack }
