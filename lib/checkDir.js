let fs = require('fs'),
    path = require('path');

function mkdir (p, j, r, cb) {
  if (p.length <= j) {
    cb && cb();
    return;
  }
  let root = r ? '/' : '';
  let p0 = root + path.join.apply(null, p.slice(0,j));
  if (p0 === '/') j+=1;
  let m = path.normalize(p0);
  fs.mkdir(path.normalize(m), () => {
    mkdir(p, j+1, r, cb);
  });
}

function mkdirSync (p, j, r) {
  if (p.length <= j) {
    return;
  }
  let root = r ? '/' : '';
  let p0 = root + path.join.apply(null, p.slice(0,j));
  if (p0 === '/') j+=1;
  let m = path.normalize(p0);
  if (m && !fs.existsSync(m)) {
    fs.mkdirSync(m);
  }
  mkdirSync(p, j+1, r);
}

function checkDir (p, cb) {
  if (!p) {
    cb('Path is undefined.');
    return;
  }
  let dir = path.dirname(p);
  if (! fs.existsSync(dir)) {
    mkdir(p.split(path.sep), 1, dir.startsWith('/'), cb);
  } else {
    cb();
  }
}

function checkDirSync (p) {
  if (!p) {
    return;
  }
  let dir = path.dirname(p);
  if (! fs.existsSync(dir)) {
    mkdirSync(p.split(path.sep), 1, dir.startsWith('/'));
  }
}

module.exports = { checkDir, checkDirSync }
