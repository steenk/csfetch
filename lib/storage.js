const 	fs = require('fs'),
		path = require('path'),
		checkDir = require('./checkDir').checkDir,
		fetch = require('node-fetch');

function store (uri, keys, list, setup, headers, cb) {
	if (typeof keys != 'object') {
		cb();
		return;
	}
	console.log('in store');
	var n = list.length || 0;
	[].concat(list).forEach((item) => {
		let savepath = keys[item._attributes.key.replace("'")];
		if (savepath) {
			let url = uri + item._attributes.url.slice(11);
			let normpath = path.normalize(process.cwd() + '/' + savepath 
				+ '/' + item._attributes.asset_id + '_'
				+ item._attributes.key + '_' + item._attributes.element_idx + '_'
				+ url.substring(url.lastIndexOf('/') + 1));
			checkDir(normpath, (err) => {
				if (!err) {
					item._attributes.original_path = normpath;
					fetch(url, {headers: headers})
					.then(res => {
						res.body.pipe(fs.createWriteStream(normpath));
						n -= 1;
						if (n === 0 && cb) {
							cb();
						}
					})
					.catch(err => {
						console.log('failed', err);
					})
				}
			})

		} else {
			n -= 1;
		}
	});
}

module.exports = { store }

