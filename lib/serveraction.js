const fs = require('fs');
const ver = require('../package.json').version;
const saver = require('./saver.js');
const status = require('./status');

function version () {
	return ver;
}

function list (config) {
	return JSON.stringify(config.files || []);
}

function issueTracker (config) {
	if (!config.setup) return '{}';
	let it = config.setup.issue_tracker;
	if (it) {
		return JSON.stringify({
			label: it.label || 'Issue Tracker',
			base_url: it.base_url || ''
		});
	}
	return '{}';
}

function save (data, config, options, cb) {
	if (data) {
		Object.keys(data).forEach(function (section) {
			let col = data[section];
			Object.keys(col).forEach(function (key) {
				if (typeof col[key] === 'object') {
					Object.keys(col[key]).forEach(function (field) {
						config[section][key][field] = col[key][field];
					})
				} else {
					config[section][key] = col[key];
				}
			})
		})
		saver.save(config, options, cb);
	}
}

function check (ws, config, cb) {
	status.check(ws, config, {}, cb);
}

module.exports = { list, version, issueTracker, save, check }

