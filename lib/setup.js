const readline = require('readline');
const save = require('./saver').save;

function config (conf, options) {
	const rl = readline.createInterface({
	  input: process.stdin,
	  output: process.stdout
	});

	var tosave = !conf;

	conf = conf || { setup: {
		username: '',
		password: '',
		server: '',
		service: '/ws/rest/service',
		mode: 'classic'
	}};
	process.stdin.read();
	let username = conf.setup.username ? '[' + conf.setup.username + '] ' : '';
	rl.question('Username? ' + username, (answer) => {
		if (answer) {
			conf.setup.username = answer;
			tosave = true;
		}

		let password = conf.setup.password ? '[' + '*'.repeat(conf.setup.password.length) + '] ' : '';
		rl.question('Password? ' + password, (answer) => {
			if (answer) {
				conf.setup.password = answer;
				tosave = true;
			}

			let server = conf.setup.server ? '[' + conf.setup.server + '] ' : '';
			rl.question('Server ("host:port")? ' + server, (answer) => {
				if (answer) {
					conf.setup.server = answer;
					tosave = true;
				}

				let service = conf.setup.service ? '[' + conf.setup.service + '] ' : '';
				rl.question('Service? ' + service, (answer) => {
					if (answer) {
						conf.setup.service = answer;
						tosave = true;
					}

					let mode = conf.setup.mode ? '[' + conf.setup.mode + '] ' : '';
					rl.question('Classic mode? ' + mode, (answer) => {
						if (answer) {
							console.log('answer', answer);
							conf.setup.mode = (answer.toLowerCase().startsWith('y') ? 'classic' : answer);
							tosave = true;
						}
						rl.question('Optional configuration? [y/N] ', (answer) => {
							if (answer.toLowerCase().startsWith('y')) {
								extra(rl, conf, options, save);
							} else {
								rl.close();
								if (tosave) {
									save(conf, options, () => {})
								}
							}
						})
					})
				})
			})
		})
	})
}

function extra (rl, conf, options, save) {
	rl.question('Pack path [censhare/censhare-Custom/censhare-Server/app/]: ', (answer) => {
		if (answer) {
			conf.setup.packpath = answer;
		}
		rl.question('Asset XML pretty print [N/y]: ', (answer) => {
			if (answer.toLowerCase().startsWith('y')) {
				conf.setup.prettyprint = true;
			}
			rl.close();
			save(conf, options, () => {});
		})
	})
}

module.exports = { config }