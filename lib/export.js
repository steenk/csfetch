
const fs = require('fs'),
    xml = require('xml-js'),
    xpath = require('xpath'),
    path = require('path'),
    mangle = require('./mangle'),
    DOMParser = require('xmldom').DOMParser,
    XMLSerializer = require('xmldom').XMLSerializer,
    DOMImplementation = require('xmldom').DOMImplementation,
    checkDir = require('./checkDir').checkDir;

var assetFileNames = {}

function prepare (config, options, cb) {
    let basedir = options.basedir || '.';
    if (! fs.existsSync(path.join(basedir, 'export'))) {
        fs.mkdirSync(path.join(basedir, 'export'));
    }

    if (!config.groups) {
        config.groups = {
            default: {
                export: '',
                subpath: ''
            }
        }
    }

    let keys = Object.keys(config.files);
    let exp = {};
    let exclexp = {};
    let group = options.group;

    keys.forEach((key) => {
        if (config.files[key].export && config.files[key].export.toLowerCase().startsWith('_no')) {
            exclexp[key] = JSON.parse(JSON.stringify(config.files[key]));
        }
        if (group && group !== 'default' && [].concat(config.files[key].group).indexOf(group) === -1) {
            return;
        }

        if (config.groups[group]) {
            exp[key] = config.files[key];
            if (!exp[key].export) {
                exp[key].export = config.groups[group].export || '';
            }
        } else if (config.files[key].export) {
            exp[key] = config.files[key];
        } else if (config.groups['default']) {
            exp[key] = {
                path: config.files[key].path,
                metadata: config.files[key].metadata
            };
            Object.keys(config.groups['default']).forEach((k) => {
                exp[key][k] = config.groups['default'][k];
            });
            if (!exp[key].export) {
                exp[key].export = config.groups['default'].export || '';
            }
        } else {
            exp[key] = config.files[key];
            if (!exp[key].export) {
                exp[key].export = '';
            }           
        }
        if (config.files[key].export_name) {
            exp[key].export_name = config.files[key].export_name;
        }

        var id = key;
        if (!/^\d+$/.test(key)) {
            fs.readFileSync(config.files[key].metadata, (err, data) => {
                let doc = new DOMParser().parseFromString(data.toString());
                id = xpath.evaluate("/asset/@asset_id", doc, null, xpath.XPathResult.ANY_TYPE, null);
            })
        }
        assetFileNames[id] = config.files[key].metadata.substr(config.files[key].metadata.lastIndexOf('/') + 1);
        if (exp[key] && exp[key].export && exp[key].export.toLowerCase().startsWith('_no')) {
            delete exp[key];
        }
    });

    Object.entries(exp).forEach((ent) => {exclexp[ent[0]] = ent[1]}); // merge exp and exclexp

    let ongoing = {};
    Object.keys(exp).forEach((key) => {
        let mypath = group && exp[key].path ?
         'storage/' + (config.groups[group]||{subpath:''}).subpath + '/' + exp[key].path.split('/').pop() :
          exp[key].path;
        ongoing[key] = true;

        fs.readFile(path.join(basedir, exp[key].metadata), (err, data) => {
            if (err) throw err;

            let doc = new DOMParser().parseFromString(data.toString());

            let uid = xpath.evaluate("/asset/asset_feature[@feature='censhare:uuid']/@value_string", 
                doc, null, xpath.XPathResult.STRING_TYPE, null).stringValue.substr(0, 8);

            const rename = !!exp[key].export_name;
            let filename = rename ? exp[key].export_name : uid + '_' + path.basename(exp[key].path||'');
            let doc2 = mangle.metadata(doc, assetFileNames, mypath, exclexp, key, filename);
            
            let p3 = exp[key].export ? 'export/' + exp[key].export + '/' : 'export/';
            checkDir(path.join(basedir, p3 + '/*'), () => {
                let p1 = p3 + filename; 
                let p2 = rename ? p3 + filename.substring(0, filename.lastIndexOf('.')) + '.asset.xml' : p3 + path.basename(exp[key].metadata);
                fs.writeFile(path.join(basedir, p2), new XMLSerializer().serializeToString(doc2), (err) => {
                    if (err) throw err;
                });
                let extra = xpath.evaluate('//storage_item', doc, null, xpath.XPathResult.ANY_TYPE, null);
                var node = extra.iterateNext();
                if (exp[key].path) {
                    fs.copyFile(path.join(basedir, exp[key].path) , path.join(basedir, p1), (err) => {
                        if (err) throw err;
                        delete ongoing[key];
                        if (Object.keys(ongoing).length === 0 && typeof cb === 'function') {
                            if (!node) cb();
                        }
                    });
                } else {
                    delete ongoing[key];
                    if (Object.keys(ongoing).length === 0 && typeof cb === 'function') {
                        if (!node) cb();
                    }
                }
                
                while (node) {
                    if ('master' !== node.getAttribute('key')) {
                        let orig = node.getAttribute('original_path');
                        node.removeAttribute('original_path');
                        if (! orig) {
                            node = extra.iterateNext();
                            continue;
                        }
                        let relpath = node.getAttribute('relpath');
                        let subpath = exp[key].export;
                        let fname = path.basename(orig);
                        let p4 = 'export/' + subpath + '/' + fname;
                        checkDir(path.join(basedir, p4), () => {
                            fs.copyFile(path.normalize(orig), path.join(basedir, p4), (err) => {
                                if (err) console.log(err);
                            })
                        })
                    }
                    node = extra.iterateNext();
                    if (!node) {
                        if (Object.keys(ongoing).length === 0 && typeof cb === 'function') {
                            cb();
                        }
                    }
                }

            })
        })
    });
}

module.exports = {prepare}
