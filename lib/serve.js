let connect = require('connect')
,   http = require('http')
,   bodyParser = require('body-parser')
,   stat = require('serve-static')
,   queries = require('connect-query')
,   url = require('url')
,   morgan = require('morgan')
,   action = require('./serveraction')
,	fetch = require('./fetchassets')
,	child_process = require('child_process');

function openBrowser (port) {
  let browse = {darwin: 'open', linux: 'xdg-open', win32: 'start', win64: 'start'};
  child_process.exec(browse[process.platform] + ' http://127.0.0.1:' + port, function(err){
    if(err) throw err;
  });
}

function serve (config, options) {
	port = config.port || '9009';

	let app = connect()
	  .use(morgan(':date :method :url :status'))
	  .use(queries())
	  .use(bodyParser.urlencoded({extended: true}))
	  .use(bodyParser.json())
	  .use(function (req, res, next) {
	    if (req.url === '/version') {
	      res.end(action.version());
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url === '/list') {
	    	//let config = require(process.cwd() + '/config.json');
	      res.setHeader('Content-Type', 'application/json');
	      res.end(action.list(config));
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url === '/setup') {
	    	//let config = require(process.cwd() + '/config.json');
	      res.setHeader('Content-Type', 'application/json');
	      res.end(JSON.stringify(config.setup||{}));
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url.indexOf('/fetch') === 0) {
	    	//let config = require(process.cwd() + '/config.json');
	     	res.setHeader('Content-Type', 'application/json');
	     	fetch.fromServer(config, res, (fetched) => {
	     		console.log('close')
	      		res.end(fetched);
	      	})
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url === '/issue_tracker') {
	    	//let config = require(process.cwd() + '/config.json');
	      res.setHeader('Content-Type', 'application/json');
	      res.end(action.issueTracker(config));
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url === '/save' && req.method === 'POST') {
	    	//let config = require(process.cwd() + '/config.json');
	      res.setHeader('Content-Type', 'application/json');
	      action.save(req.body, config, options, function (r) {
	      	res.end(JSON.stringify(r));
	      });
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url === '/check_status') {
	      res.setHeader('Content-Type', 'text/plain');
	      action.check(res, config, (r) => {
	      	console.log(r)
	      	res.end();
	      });
	    } else {
	      next();
	    }
	  })
	  .use(function (req, res, next) {
	    if (req.url === '/exit') {
	      res.end();
	      process.exit();
	    } if (req.url === '/status') {
	      res.write('Running on port ' + port + ' with root ' + root + '\n');
	      res.end();
	    } else {
	      next();
	    }
	   })
	  .use(stat(__dirname +'/webroot'));

	http.createServer(app).listen(port, "127.0.0.1");
	openBrowser(port);
}

module.exports = { serve }
