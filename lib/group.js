const fs = require('fs'),
	  save = require('./saver').save;

function groups (config, options) {
	if (!config) throw 'No config!';
	if (!config.groups) {
		config.groups = {
			default: {
				export: '',
				subpath: ''
			}
		}
	}
	let keys = Object.keys(config.groups);
	var group = options.group;
	if (group) {
		let args = options.groupargs;
		if (/=/.test(args[0])) {
			group = 'default'
		}
		if (!config.groups[group]) {
			config.groups[group] = {
				export: '',
				subpath: ''
			}
		}
		args.forEach((arg) => {
			let [key, val] = arg.split('=');
			config.groups[group][key] = val;
		});
		save(config, options, () => {})
		//fs.writeFileSync(process.cwd() + '/config.json', JSON.stringify(config, null, ' '));
	} else {
		keys.forEach((key) => {
			console.log(key, JSON.stringify(config.groups[key]));
		})
	}
	
}



module.exports = {groups}