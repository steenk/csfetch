const xml = require('xml-js'),
      xpath = require('xpath'),
      path = require('path'),
	  DOMImplementation = require('xmldom').DOMImplementation;

const oppo = {
	'parent_asset': 'child_asset',
	'child_asset': 'parent_asset'
}

// node is the document node with a relation in it
// attr is either 'child_asset' or 'parent_asset'
// assetFileNames has the file names of all fetched metadata
// exp is the part of config.json with only assets to export
function prepareRelation (node, attr, assetFileNames, exp) {
	let ref = node.getAttribute(attr); // ref to parent or child
	let id = node.getAttribute(oppo[attr]); // asset id of counter part
	if (!assetFileNames[ref]) {
		console.log('Warning, ' + attr + ' ' + ref + ' is not fetched yet! Relationship type is ' + node.getAttribute('key'));
		return false;
	} else {
		var rel;
		if (id && ref) {
			rel = relPath(exp[ref].export, exp[id].export);
		}
		rel = rel ? rel + '/' : '';
		node.removeAttribute(attr);
		// use resource key if asset has one
		if (ref && exp[ref].resourcekey) {
			node.setAttribute(attr + '_ref_resource_key', exp[ref].resourcekey);
		} else {
			node.setAttribute(attr + '_ref_file', rel + assetFileNames[ref]);
		}
		//console.log('Relation ' + attr + ' ' + assetFileNames[ref]);
		node.removeAttribute('rowid');
		node.removeAttribute('sid');
		let assetFeature = xpath.evaluate(`./node()[
			name() = 'asset_rel_feature']`, node, null, xpath.XPathResult.ANY_TYPE, null);
		
		let subnode = assetFeature.iterateNext();
		while (subnode) {
			if (subnode.getAttribute('feature') === 'censhare:module-rel') {
				node.removeChild(subnode);
			} else {
				subnode.removeAttribute('rowid');
				subnode.removeAttribute('sid');
			}
			subnode = assetFeature.iterateNext();
		}
		return true;
	}
}

function relPath (p1, p2) {
  let s1 = p1.split('/');
  let s2 = p2.split('/');
  while (true) {
    if (s1[0] === s2[0]) {
      s1.shift();
      s2.shift();
      if (!s1.length) break;
    } else {
      break;
    }
  }
  let s3 = [];
  s2.forEach(() => {
    s3.push('..');
  });
  s3 = s3.concat(s1);
  return s3.join('/');
}

function metadata (doc, assetFileNames, mypath, exp, key, filename) {
	let asset = doc.documentElement;
	//console.log(doc.toString());
	//let uid = xpath.evaluate("/asset/asset_feature[@feature='censhare:uuid']/@value_string", 
	//	doc, null, xpath.XPathResult.STRING_TYPE, null).stringValue.substr(0, 8);

	let doc2 = new DOMImplementation().createDocument(null, "asset");

	let parents = xpath.evaluate('/asset/parent_asset_rel', doc, null, xpath.XPathResult.ANY_TYPE, null);
	var node = parents.iterateNext();
	while (node) {
		if (prepareRelation(node, 'parent_asset', assetFileNames, exp)) {
			prepareRelation(node, 'child_asset', assetFileNames, exp);
			doc2.documentElement.appendChild(node);
		}
		node = parents.iterateNext();
	}

	let children = xpath.evaluate('/asset/child_asset_rel', doc, null, xpath.XPathResult.ANY_TYPE, null);
	var node = children.iterateNext();
	while (node) {
		if (prepareRelation(node, 'child_asset', assetFileNames, exp)) {
			prepareRelation(node, 'parent_asset', assetFileNames, exp);
			doc2.documentElement.appendChild(node);
		}
		node = children.iterateNext();
	}


	let assetFeature = xpath.evaluate(`/asset/node()[
		name() != 'storage_item' and
		name() != 'parent_asset_rel' and
		name() != 'child_asset_rel']`, doc, null, xpath.XPathResult.ANY_TYPE, null);

	doc2.documentElement.setAttribute('name', asset.getAttribute('name'));
	doc2.documentElement.setAttribute('application', asset.getAttribute('application'));
	doc2.documentElement.setAttribute('type', asset.getAttribute('type'));
	doc2.documentElement.setAttribute('domain', asset.getAttribute('domain'));
	doc2.documentElement.setAttribute('domain2', asset.getAttribute('domain2'));
	doc2.documentElement.setAttribute('language', asset.getAttribute('language'));
	doc2.documentElement.setAttribute('description', asset.getAttribute('description'));
	node = assetFeature.iterateNext();
	while (node) { 
		if (!node.tagName) {
			node = assetFeature.iterateNext();
			continue;
		}
		if (node.tagName !== 'message' &&
				!(node.tagName === 'asset_feature' && node.getAttribute('feature') === 'censhare:module-asset-source')) {
			node.removeAttribute('asset_id');
			node.removeAttribute('asset_currversion');
			node.removeAttribute('asset_version');
			node.removeAttribute('deletion');
			node.removeAttribute('rowid');
			node.removeAttribute('sid');
			node.removeAttribute('timestamp');
			node.removeAttribute('party');
			let valueId = node.getAttribute('value_asset_id');
			if (valueId) {
				let resourceKey = exp[valueId] && exp[valueId].resourcekey;
        console.log(mypath, resourceKey);
				if (resourceKey) {
					node.removeAttribute('value_asset_id');
					node.setAttribute('value_asset_id_ref_resource_key', resourceKey);
				} else {
					console.log('Warning! Feature is referring to asset with id = ' + valueId + ', but referenced asset has no resource key!');
				}
			}	
			doc2.documentElement.appendChild(node);
		}
		node = assetFeature.iterateNext();
	}
	let itemKeys = ['master'].concat(Object.keys(exp[key].storage||{}));
	var attr, storage;
	for (itemKey of itemKeys) {
		let storageItem = xpath.evaluate(`//storage_item[@key='${itemKey}']`,
				doc, null, xpath.XPathResult.ANY_TYPE, null);
		node = storageItem.iterateNext();
		while (node) {

			var relpath = mypath;
			let storage = doc2.createElement('storage_item');
			let attributes = xpath.evaluate(`@*[name()='key' or name()='mimetype' or name()='app_version' 
				or name()='element_idx' or name()='original_path']`,
					node, null, xpath.XPathResult.ANY_TYPE, null);
			attr = attributes.iterateNext();
			while (attr) {
				if (attr && attr.name !== 'original_path') {
					storage.setAttribute(attr.name, attr.value);
				} else {
					relpath = attr.value;
 				}
 				attr = attributes.iterateNext();
			}
			storage.setAttribute('relpath', 'file:' + (filename || path.basename(relpath)));

			doc2.documentElement.appendChild(storage);
			node = storageItem.iterateNext();
		}
	}

	return doc2;
}

module.exports = { metadata }
