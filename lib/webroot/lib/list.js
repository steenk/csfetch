import $$$, { destroy } from  "./tripledollar.mjs";
import { plus, link } from  "./icons.js";
import save from "./save.js";

var cardUrl = '';

const fields = {
	metadata: 'Metadata',
	path: 'Path',
	group: 'Group',
	export: 'Export',
	comment: 'Comment',
	card: 'Issue Tracker',
	resourcekey: 'Resource Key',
	storage: 'Storage'
}

var changes = {}

fetch('/issue_tracker')
	.then((r) => r.json()
		.then((data) => {
			let url = data.base_url || '';
			cardUrl = url.endsWith('/') ? url : url + '/';
			fields.card = data.label || 'Card';
		})
	);

function addField (evt, item) {
	evt.stopPropagation();
	let key = evt.target.getAttribute('value');
	let plus = item.query('.plus');
	let list = item.query('.option-list');
	destroy(list);
	item.removeChild(list);
	//item.insertBefore($$$(['div.row', {field: key}, ['div.label', fields[key] + ':'], ['div.value', {contenteditable:true}]]), plus);
	item.insertBefore(activeRow(key), plus);
}

function plusAction (evt, key) {
	evt.stopPropagation();
	if (closePopups()) return;
	let item = document.getElementById('id-' + key);
	let popup = $$$(['ul.option-list']);
	let rows = item.queryAll('.row');
	let exists = {};
	rows.forEach(function (row) {
		exists[row.getAttribute('field')] = true;
	});
	Object.keys(fields).forEach(function (field) {
		if (! exists[field]) {
			popup.ins($$$('li', {value: field}, fields[field]));
		}
	});
	if (Object.keys(exists).length < Object.keys(fields).length) {
		popup.evt('click', addField, item);
		item.ins(popup);
	}
}

function closePopups (evt) {
	let pops = document.querySelectorAll('.option-list');
	let found = !!pops.length;
	pops.forEach(function (popup) {
		destroy(popup);
		popup.parentElement.removeChild(popup);
	});
	return found;
}

let groupSel = ['select.group'];

function groupSelector (allGroups) {
	let list = Object.keys(allGroups).sort();
	list.forEach((g) => {
		groupSel.push(['option', {value: g}, g]);
	});
}

function filterRow (data, list) {
	let div = $$$('div.filter',
		['label', 'Excluded from export ', ['input#excluded-assets', {type:'checkbox'}]]
	).evt('change', function (evt) {
		let filter = {}
		if (evt.target.id === 'excluded-assets') {
			filter.excludedAssets = evt.target.checked;
		}
		//console.log('filter', JSON.stringify(filter));
		renderRows(data, list, filter);
	});
	return div;
}

function listing (data) {
	let list = $$$('div.listing');
	let div = $$$('div', filterRow(data, list));
	div.ins(list);
	list.evt('click', closePopups);
	list.evt('change', function (evt) {
		let val = evt.target.textContent;
		let field = evt.target.parentElement.getAttribute('field');
		let key = evt.target.parentElement.parentElement.id.replace(/^id-/, '');
		let chg = {};
		chg[key] = {}
		chg[key][field] = val;
		save.prepare({files: chg});
		data[key][field] = val;
	})
	let allGroups = {};
	Object.keys(data).forEach(function (key) {
		let groups = [].concat(data[key].group || '');
		groups.forEach((g) => {allGroups[g] = true});
	});
	renderRows(data, list, {});
	groupSelector(allGroups);
	return div;
}

function activeRow (key, value) {
	value = value || '';
	//let listing = document.querySelector('.listing');
	let row = $$$(['div.row', {field: key}, ['div.label', fields[key] + ':'],
		$$$('div.value', {contenteditable:true}, value).evt('keyup', function (evt) {
			let event = new Event('change', {bubbles: true});
			evt.target.dispatchEvent(event);
		})
	]);
	return row;
}

function passiveRow (key, value) {
	value = value || '';
	let row = $$$(['div.row', {field: key}, ['div.label', fields[key] + ':'],
		$$$('div.value', value).evt('keyup', function (evt) {
			let event = new Event('change', {bubbles: true});
			evt.target.dispatchEvent(event);
		})
	]);
	return row;
}

function url (a, b) {
	return a + b;
}

function listProps (obj, valueType) {
	valueType = valueType ? valueType + ': ' : '';
	if (typeof obj !== 'object') return;
	let keys = Object.keys(obj);
	let div = $$$('div');
	keys.forEach((key) => {
		div.ins(['div.list-prop',
			['span.bold', key],
			['span', ' ', valueType],
			['span', obj[key]]
		]);
	});
	return div;
}

function renderRows (data, list, filter) {
	destroy(list);
	Object.keys(data).forEach(function (key) {
		if (filter.excludedAssets && 
			!(data[key].export && 
			data[key].export.toLowerCase().startsWith('_no'))) {
			return;
		}
		else if (!filter.excludedAssets && (data[key].export && 
			data[key].export.toLowerCase().startsWith('_no'))) {
			return;
		}
		let groups = [].concat(data[key].group || '');
		let item = $$$('div.item#id-' + key);
		item.ins(
			['h3', key],
			['div.row', {field: 'metadata'}, ['div.label', 'Metadata:'], ['div.value', data[key].metadata]],
			['div.row', {field: 'path'}, ['div.label', 'Path:'], ['div.value', data[key].path || '']]);

		item.ins(['div.row', {field: 'group'}, ['div.label', 'Group:'],  ['div.value', groups.join(', ')]]);
		
		if (data[key].storage) {
			item.ins(passiveRow('storage', listProps(data[key].storage, 'path')));
			//item.ins(['div.row', {field: 'resourcekey'}, ['div.label', fields.resourcekey + ':'], ['div.value', data[key].resourcekey]]);
		}
		if (data[key].resourcekey) {
			item.ins(activeRow('resourcekey', data[key].resourcekey));
			//item.ins(['div.row', {field: 'resourcekey'}, ['div.label', fields.resourcekey + ':'], ['div.value', data[key].resourcekey]]);
		}
		if (data[key].export) {
			item.ins(activeRow('export', data[key].export));
			//item.ins(['div.row', {field: 'export'}, ['div.label', fields.export + ':'], ['div.value', {contenteditable:true}, data[key].export]]);
		}
		if (data[key].card) {
		 	item.ins(['div.row', {field: 'card'}, ['div.label', fields.card + ':'], ['div.card-key', {contenteditable:true}, data[key].card], ' ',
			 ['a', {href: url(cardUrl, data[key].card), target:'_blank'},
			  link()]]);
		}
		if (data[key].comment) {
			item.ins(activeRow('comment', data[key].comment));
			//item.ins(['div.row', {field: 'comment'}, ['div.label', fields.comment + ':'], ['div.value', {contenteditable:true}, data[key].comment]]);
		}
		item.ins(plus().evt('click', plusAction, key));
		list.ins(item);
	});
}


export { listing }