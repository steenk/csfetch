import $$$, { destroy } from  "./tripledollar.mjs";
import save from "./save.js";

function setup () {
	let div = $$$.query('.setup');
	destroy(div);
	fetch('/setup')
	.then(r => r.json())
	.then(conf => {
		div.ins(form(conf));
	})
}

function change (evt) {
	console.log(evt.target.id, evt.target.value);
	let key = evt.target.id.replace(/^setup-/, '');
	let val = evt.target.value;
	let prep = {setup:{}};
	prep.setup[key] = val;
	save.prepare(prep);
	let but = document.querySelector('.save-button');
	but.style.backgroundColor = 'red';
}

function activeRow (key, value, label) {
	value = value || '';
	let row = $$$(['div.row', {field: key}, ['div.label', label + ':'],
		$$$('input#setup-' + key, {type:'text',value: value}).evt('keyup', function (evt) {
			let event = new Event('change', {bubbles: true});
			evt.target.dispatchEvent(event);
		})
	]);
	return row;
}

function form (conf) {
	conf = conf || {
		username: '',
		password: '',
		server: '',
		service: '/ws/rest/service'
	};
	let fields = [
		{key: 'username', value: conf.username, label: 'Username'},
		{key: 'password', value: '*'.repeat(conf.password.length), label: 'Password'},
		{key: 'server', value: conf.server, label: 'Server'},
		{key: 'service', value: conf.service, label: 'Service'}
	];

	let div = $$$('div').evt('change', change);
	var field;
	for (field of fields) {
		div.ins(activeRow(field.key, field.value, field.label))
	}

	return div;
}

export default { setup }
