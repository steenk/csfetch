
var prep = {}

function prepare (obj) {
	// sections are treated differently
	var section;
	if (obj && obj.files) {
		section = 'files';
		!(typeof prep[section] === 'object') && (prep[section] = {});
		Object.keys(obj.files).forEach(function (key) {
			!prep[section][key] && (prep[section][key] = {});
			Object.keys(obj.files[key]).forEach(function (field) {
				prep[section][key][field] = obj.files[key][field];
			})
		})
	}
	if (obj && obj.setup) {
		section = 'setup';
		!(typeof prep[section] === 'object') && (prep[section] = {});
		Object.keys(obj[section]).forEach(function (key) {
			prep[section][key] = obj[section][key];
		});
	}
	let but = document.querySelector('.save-button');
	but.style.backgroundColor = 'red';
}

function apply () {
	let blob = new Blob([JSON.stringify(prep)], {type : 'application/json'});
	fetch('/save', {
		method: 'POST',
		body: blob
	}).then(r => {
		console.log(r);
		prep = {};
		let but = document.querySelector('.save-button');
		but.style.backgroundColor = '';
	});
}


export default { prepare, apply }