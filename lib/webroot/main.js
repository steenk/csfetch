import $$$, { destroy } from  "./lib/tripledollar.mjs";
import { listing } from "./lib/list.js";
import { power } from  "./lib/icons.js";
import save from "./lib/save.js";
import setup from "./lib/setup.js";

let fetchButton = $$$('div.button.fetch-button', 'Fetch').evt('click', function () {
	let items = $$$.queryAll('.item');
	for (let i=0; i<items.length; i+=1) {
		items.item(i).classList.add('waiting');
	}
	console.log('click')
	fetch('/fetch').then(r => {
		let dec = new TextDecoder("utf-8");
		var n = 0;

		return new Response(
			new ReadableStream({
				start(ctl) {
					let rdr = r.body.getReader();
					read();
					function read () {
						rdr.read().then(d => {
							if (d.done) {
								ctl.close();
								return;
							}
							let s = dec.decode(d.value);
							if (s) {
								for (let row of s.split('\n')) {
									let aid = /^\d+/.exec(row);
									if (aid) {
										let id = 'id-' + aid[0];
										let itm = document.getElementById(id);
										if (itm) {
											itm.classList.remove('waiting');
										}
									}
								}
							}
							ctl.enqueue(d.value);
							read();
						})
					}
				}
			})
		);
	})
	.catch((err) => {
		console.log(err);
	})
});
let saveButton = $$$('div.button.save-button', 'Apply changes').evt('click', function () {
	let divs = $$$.queryAll('.close-on-save');
	for (let i=0; i<divs.length; i+=1) {
		destroy(divs.item(i));
		divs.item(i).classList.remove('open');
	}
	save.apply();
});
let statusButton = $$$('div.button.status-button', 'Status').evt('click', function () {
	let status = $$$.query('.status');
	if (status.classList.contains('open')) {
		status.classList.remove('open');
		destroy(status);
		return;
	}
	fetch('/check_status')
	.then((r) => r.text()
		.then((d) => {
			destroy(status);
			status.classList.add('open');
			d.split('\n').forEach((t) => {
				status.ins(['p', t]);
			})
		})
	)
});
let setupButton = $$$('div.button.setup-button', 'Setup').evt('click', function () {
	let setupButton = $$$.query('.setup');
	if (setupButton.classList.contains('open')) {
		setupButton.classList.remove('open');
		destroy(setupButton);
		return;
	} else {
		setupButton.classList.add('open');
		setup.setup();
	}
});


function powerOff () {
	fetch('/exit')
	.then(() => {
		//destroy(document.body);
		window.close();
	})
}

$$$.appendToDoc(['div.black',
	['img', {alt:'csfetch', src:'img/csfetch_logo.png'}],
	//$$$('img.nice-dog', {src:'img/csfetch.svg'}).evt('click', niceDog),
		['p.version'],
		['div.right-control',
			power().evt('click', powerOff)
		]
	],
	['div.control-list',
		saveButton, fetchButton, statusButton, setupButton
	],
	['div.status'],
	['div.setup.close-on-save'],
	['div.board']
)
.then(
	(function () {
	fetch('/list')
	.then((r) => r.json())
	.then((d) => {
		let board = $$$.query('.board');
		board.ins(['h2', 'Asset list']);
		board.ins(listing(d));
	})
	.catch(err => console.log);
	fetch('/version')
	.then((r) => r.text()
		.then((v) => {
			$$$.query('.version').textContent = 'Version ' + v;
		})
	)
	}())
);

function niceDog (evt) {
	let dog = document.querySelector('.nice-dog');
	dog.style = 'display: none;';
}