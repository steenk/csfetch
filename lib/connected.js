const xml = require('xml-js'),
	  fetch = require('node-fetch'),
      https = require('https');

function connected (config, options, cb) {
	let url = `https://${config.setup.server}${config.setup.service}/assets/all;limit=0`;
	let target = options.target || 'default';
	let headers = {Authorization: config._temp[target].auth};

	const agent = new https.Agent({
  		rejectUnauthorized: false
	});

	fetch(url, {agent, headers})
	.then(r => r.buffer())
	.then(data => {
      if (String(data).indexOf('<assets') > 30) {
      	cb('yes')
      } else {
      	cb('no')
      }
	})


/*  https.get(url, { rejectUnauthorized: false, headers }, (res) => {
    res.on('data', (data) => {
      if (String(data).indexOf('<assets') > 30) {
      	cb('yes')
      } else {
      	cb('no')
      }
    })
  })
  */
}

module.exports = { connected }