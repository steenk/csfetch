const fs = require('fs'),
      xml = require('xml-js'),
      fetch = require('node-fetch'),
      path = require('path'),
      { checkDir, checkDirSync } = require('./checkDir'),
      store = require('./storage').store;

const uuidre = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;

function getStorageItem (list, key) {
  var pos;
  if (!Array.isArray(list)) {
    if (list?._attributes.key === key) {
      return list;
    }
  }
  list?.forEach((item, i) => {
    if (item._attributes.key === key) {
      pos = i;
    }
  })
  return list && list[pos];
}

function fromServer (config, ws, options, cb) {
  ws = ws || process.stdout;
  function log (data) {
    ws.write(String(data));
    ws.write('\n');
  }
  if (options.target) {
    console.log('target is', options.target, config.targets[options.target]);
  }
  config.files || (config.files = {});
  checkDirSync('assets/*');
  checkDirSync('storage/*');
  let fetched = {};
  let headers = {Authorization: config.auth};
  let group = process.argv[3] ? undefined : process.argv[2];
  if (group === 'serve') group = undefined;

  if (config.setup.chunks) { // delay each 10th
    let chunks = config.setup.chunks;
    let c = [];
    let a = Object.keys(config.files);
    while (a.length) {  c.push(a.splice(0,chunks)) }
    function next () {
      if (!c.length) return;
      setTimeout(() => {
        let f = c.shift();
        next();
        runFilelist(f, group, config, headers, fetched, log, cb);
      }, 1000);
    }
    next();
  } else {
    runFilelist(Object.keys(config.files), group, config, headers, fetched, log, cb);
  }
}

function runFilelist (filelist, group, config, headers, fetched, log, cb) {
  filelist.forEach((key, idx) => {
    if (group && [].concat(config.files[key].group).indexOf(group) === -1) {
      return;
    }
    let metadata = path.normalize(process.cwd() + '/' + config.files[key].metadata);
    if (config.files[key].metadata) {
      //console.log('checking', metadata)
      checkDir(metadata, () => {
          //console.log('ok', metadata)

        let ckey = encodeURI(key);

        let url = (/^\d+$/.test(key)) ?
          `https://${config.setup.server}${config.setup.service}/assets/asset/id/${ckey}` :
          (uuidre.test(key) ?
            `https://${config.setup.server}${config.setup.service}/assets/asset/uuid/${ckey}` : 
            `https://${config.setup.server}${config.setup.service}/assets/asset;censhare:resource-key=${ckey}`);

        fetch(url, {headers: headers})
          .then(res => res.buffer())
          .then(res => {
            let aj = JSON.parse(xml.xml2json(res, {compact: true}));
            if (!aj.html && aj.asset && aj.asset._attributes) {
              if (fetched[aj.asset._attributes.id]) {
                  console.log(aj.asset._attributes.id + ') is a duplicate ' + data.length);
                  //cb && (cb());
              } else {
                fetched[aj.asset._attributes.id] = true;
                if (!config.files[key].path) {
                  let uri = 'https://' + config.setup.server;
                  let items = aj.asset.storage_item;
                  store(uri, config.files[key].storage, items, config.setup, headers, () => {
                    console.log('got storage items for', aj.asset._attributes.id)
                  });
                  let options = {compact: true, ignoreComment: true, spaces: 0};
                  if (config.setup.prettyprint) {
                    options.spaces = 4;
                  }
                  fs.writeFile(metadata, xml.json2xml(aj, options), (err) => {
                    if (err) log(err);
                    log(key);
                  });
                  return;
                }

                let url = (/^\d+$/.test(key)) ?
                  `https://${config.setup.server}${config.setup.service}/assets/asset/id/${key}/storage/master/file` :
                  (uuidre.test(key) ?
                    `https://${config.setup.server}${config.setup.service}/assets/asset/uuid/${key}/storage/master/file` : 
                    `https://${config.setup.server}${config.setup.service}/assets/asset;censhare:resource-key=${key}/storage/master/file`);

                let filepath = path.normalize(group ? 'storage/' + config.groups[group].subpath + '/' + config.files[key].path.split('/').pop() : config.files[key].path);
                fetch(url, {headers: headers})
                .then(res => {
                  res.body.pipe(fs.createWriteStream(filepath));
                  cb && cb();
                })
                .catch(console.log);
                let uri = url.split('/service/assets/asset/id/')[0];
                let items = aj.asset.storage_item;

                store(uri, config.files[key].storage, items, config.setup, headers, () => {
                  let options = {compact: true, ignoreComment: true, spaces: 0};
                  let master = getStorageItem(aj.asset.storage_item, 'master');
                  if (master) {
                    master._attributes.original_path = path.normalize(process.cwd() + '/' + filepath);
                  } else {
                    console.log('NO MASTER FOUND')
                  }
                  fs.writeFile(metadata, xml.json2xml(aj, options), (err) => {
                    if (err) log(err);
                    log(key);
                  });
                });

              }
            }

          })
          .catch(console.log);
         
       })
     }
  })
}

module.exports = { fromServer }

