const fs = require('fs'),
    xml = require('xml-js'),
    fetch = require('node-fetch'),
    path = require('path'),
    https = require('https'),
    checkDir = require('./checkDir').checkDir,
    checkDirSync = require('./checkDir').checkDirSync,
    save = require('./saver').save;

const uuidre = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;

const agent = new https.Agent({
    rejectUnauthorized: false
});

function mangleName (name, suf) {
    return name.replace(/\s+/g, '_')
            .replace('&', '+')
            .replace(':', '')
            .replace('|', '')
            .replace(new RegExp((suf || '') + '$'), '');
}

function suffix (url) {
  let r =  /\.[^\.]+$/g.exec(url);
  return (r && r[0]) || '.bin';
}

function arr2obj (entry) {
    let hash = {};
    if (Array.isArray(entry)) {
        entry.forEach((item) => {
            hash[item._attributes.feature||item._attributes.key] = item;
        })
    } else if (entry) {
        hash[entry._attributes.feature||entry._attributes.key] = entry;
    }
    return hash;
}

function addOne (config, options, props, cb) {
    if (props) {
        toConfig(config, options, props, cb);
    } else {
        if (! options.ids.length >= 1) {
            console.log('Need an id or a resource key to add to the "config.json" file.');
            process.exit();
        }
        let [key, ...extra] = options.ids;
    
        props = {key: key};
        extra.forEach((arg) => {
            if (arg.indexOf('=') > 0) {
                let [prop, val] = arg.split('=');
                if (prop === 'export' || prop === 'path' || prop === 'card' || prop === 'comment') {
                    props[prop] = val;
                }
            } else {
                props.group = arg;
            }
        });
        toConfig(config, options, props, cb);
    }
}

function toConfig (config, options, props, cb) {
    let include_key = !(config.setup.mode === 'classic');
    let key = props.key;
    options.target || (options.target = 'default');
    delete props.key;
    if (!config) throw 'No config!';

    !config.files && (config.files = {});
    
    if (config.files[key]) {
        console.log('Entry exists already!');
        let group = props.group;
        if (group) {
            let exgrp = config.files[key].group;
            if (!exgrp) {
                config.files[key].group = group;
            } else if (Array.isArray(exgrp)) {
                if (exgrp.indexOf(group) === -1) {
                    config.files[key].group = exgrp.concat([group]);
                }
            } else if (exgrp && exgrp !== group) {
                config.files[key].group = [exgrp].concat([group]);
            }
            delete config.auth;
            save(config, options, options, (err) => {
                if (err) throw err;
                console.log('Added group.');
                process.exit();
            })
        } else {
            process.exit();
        }
    }

    checkDirSync(options.basedir + '/assets/*');
    //if (! fs.existsSync(options.basedir + '/assets')) {
    //    fs.mkdirSync(options.basedir + '/assets');
    //}

    let headers = {Authorization: config._temp[options.target].auth};

    let url = (/^\d+$/.test(key)) ?
              `https://${config.setup.server}${config.setup.service}/assets/asset/id/${key}` :
              (uuidre.test(key) ?
                `https://${config.setup.server}${config.setup.service}/assets/asset/uuid/${key}` : 
                `https://${config.setup.server}${config.setup.service}/assets/asset;censhare:resource-key=${key}`);
    fetch(url, {headers, agent})
    .then(res => res.buffer())
    .then(body => {
        let aj = JSON.parse(xml.xml2json(body, {compact: true}));

        if (!aj.html && aj.asset && aj.asset._attributes) {
            let key = aj.asset._attributes.id;
            let features = arr2obj(aj.asset.asset_feature);
            let entry = {
                metadata: path.join('assets', features['censhare:uuid']._attributes.value_string + '.asset.xml')
            }
            if (props.group) {
                entry.group = props.group;
                console.log('Added group.');
            }
            if (props.export) {
                entry.export = props.export;
                console.log('Added export.');
            }
            if (props.path) {
                entry.path = props.path;
                console.log('Added path.');
            }
            if (props.card) {
                entry.card = props.card;
                console.log('Added card.');
            }
            if (props.comment) {
                entry.comment = props.comment;
                console.log('Added comment.');
            }
            let resourcekey = features['censhare:resource-key'];
            if (resourcekey) {
                entry.resourcekey = resourcekey._attributes.value_asset_key;
            }

            let metapath = path.normalize(path.join(options.basedir, entry.metadata));
            checkDirSync(metapath);
            if (config.setup.prettyprint) {
                 body = xml.json2xml(aj, {compact: true, spaces: 4});
            }
            fs.writeFile(metapath, body, (err) => {
                if (err) throw err;
                console.log('Written ' + entry.metadata);
                if (props.path && props.path.toLowerCase().startsWith('_no')) {
                    config.files[key] = entry;
                    delete config.auth;
                    save(config, options, (err) => {
                        if (err) throw err;
                            console.log('New entry for asset-id ' + key + ' added to config.json.');
                        });
                    return;
                }
                let storage_items = arr2obj(aj.asset.storage_item);
                if (storage_items.master) {
                    let group = entry.group || 'default';
                    var subpath;
                    if (config.groups && config.groups[group]) {
                        subpath = (config.groups[group].subpath || '').replace(/^\//, '').replace(/\/$/, '');
                    }
                    let spath = 'storage/' + (subpath ? subpath + '/' : '');
                    let suf = suffix(storage_items.master._attributes.relpath);
                    let storage_file = path.join(spath, mangleName(aj.asset._attributes.name, suf) + (include_key ? '.' + key : '') + suf);
                    checkDir(path.join(options.basedir, storage_file), (err) => {
                        if (err) throw err;
                        entry.path = path.posix.normalize(storage_file).replace(/\\/g, '/');
                        let surl = (/^\d+$/.test(key)) ?
                          `https://${config.setup.server}${config.setup.service}/assets/asset/id/${key}/storage/master/file` :
                          (uuidre.test(key) ?
                            `https://${config.setup.server}${config.setup.service}/assets/asset/uuid/${key}/storage/master/file` : 
                            `https://${config.setup.server}${config.setup.service}/assets/asset;censhare:resource-key=${key}/storage/master/file`);

                        fetch(surl, {headers, agent})
                        .then(res => res.buffer())
                        .then(body => {
                            if (err) throw err;
                            fs.writeFile(path.normalize(path.join(options.basedir, entry.path)), body, (err) => {
                                if (err) throw err;
                                console.log('Written ' + entry.path);
                                config.files[key] = entry;
                                delete config.auth;
                                save(config, options, (err) => {
                                    if (err) throw err;
                                    console.log('New entry for asset-id ' + key + ' added to config.json.');
                                })
                            });
                            cb && cb();
                        })
                        .catch(err => {})
                    })
                } else {
                    config.files[key] = entry;
                    delete config.auth;
                    save(config, options, (err) => {
                        if (err) throw err;
                        console.log('New entry for asset-id ' + key + ' added to config.json.');
                        cb && cb();
                    })
                }
            })
        }

    })
    .catch(err => {throw err});
}

module.exports = {toConfig, addOne}


