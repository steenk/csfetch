const fs = require('fs'),
	  path = require('path');

function save (data, options, cb) {
	let json = JSON.stringify(data);
	let jsoncopy = JSON.parse(json);
	delete jsoncopy._temp;
	json = JSON.stringify(jsoncopy, null, ' ');
	
	if (typeof json !== 'undefined') {
		if (fs.existsSync(options.config)) {
			fs.copyFile(options.config, '.config_bu.json', null, (err) => {
				if (err) throw err;
				fs.writeFile(options.config, json, (err) => {
					if (err) throw err;
					cb && cb('');
				});
			});
		} else {
			fs.writeFile(options.config, json, (err) => {
				if (err) throw err;
				cb && cb('');
			});
		}
	} else {
		cb && cb('Error while saving config.json!');
	}
}

module.exports = { save }
