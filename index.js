const fs = require('fs'),
      path = require('path'),
      version = require('./package.json').version,
      isConnected = require('./lib/connected.js').connected,
      add = require('./lib/add.js'),
      svr = require('./lib/server.js'),
      stat = require('./lib/status.js'),
      exp = require('./lib/export.js'),
      pack = require('./lib/pack.js');

function toBase64 (str) {
  return Buffer.from(str || '', 'utf8').toString('base64')
}

function authHash (username, password) {
  var header = username + ':' + (password || '')
  let authHeader = 'Basic ' + toBase64(header)
  return authHeader;
}

function init (folder) {
  folder || (folder = '.');
  if (!folder || !fs.existsSync(folder)) throw "Folder doesn't exist";
  this.folder = folder;
  if (path.isAbsolute(folder)) {
    this.basedir = path.normalize(folder);
  } else {
    this.basedir = path.normalize(process.cwd() + '/' + folder);
  }

  this.config = {
     setup: {},
     files: {}
  }
  if (fs.existsSync(path.join(folder, 'config.json'))) {
    this.config = JSON.parse(fs.readFileSync(path.join(folder, 'config.json')));
    if (this.config.setup && this.config.setup.hasOwnProperty('username') && this.config.setup.hasOwnProperty('password')) {
      this.config._temp || (this.config._temp = {default:{}});
      this.config._temp.default.auth = authHash(this.config.setup.username, this.config.setup.password);
    }
    if (this.config.target) {
      Object.keys((key) => {
          this.config._temp[key] || (this.config._temp[key] = {});
          this.config._temp[key].auth = authHash(this.config.target[key].username, this.config.target[key].password);
      })
    }
  //} else {
    //fs.writeFileSync(path.join(folder, 'config.json'), JSON.stringify(this.config));
  }
  return this.config;
}

function saveConfig () {
  let conf = JSON.parse(JSON.stringify(this.config));
  delete conf._temp;
  fs.writeFileSync(path.join(this.basedir, 'config.json'), JSON.stringify(conf, null, ' '));
  return true;
}

function connected () {
  let p = new Promise((resolve, reject) => {
    isConnected(this.config, {target:'default'}, (res) => {
      if (res === 'yes') {
        resolve(res);
      } else {
        reject(res);
      }
    })
  })
  return p;
}

function setup (props, saveit = false) {
  props || (props = this.config.setup);
  const defaultValues = {
    service: '/ws/rest/service',
    mode: 'classic'
  };
  ['username', 'password', 'server', 'service', 'mode'].forEach(prop => {
    this.config.setup[prop] = props[prop] || defaultValues[prop] || '';
  });
  this.config._temp || (this.config._temp = {default:{}});
  this.config._temp.default.auth = authHash(this.config.setup.username, this.config.setup.password);
  saveit && saveConfig();
}

function addTarget (props) {
  let keys = ['key', 'path', 'server', 'username', 'password'];
  keys.forEach(prop => {
    if (typeof props[prop] === 'undefined') throw 'Undefined propery';
  })
  props.service || (props.service = '/ws/rest/service');
  keys.shift();
  this.config.target || (this.config.target = {});
  this.config.target[props.key] || (this.config.target[props.key] = {});
  keys.forEach(k => {
    this.config.target[props.key][k] = props[k];
  })
  this.config._temp[props.key] || (this.config._temp[props.key] = {});
  this.config._temp[props.key].auth = authHash(props.username, props.password);
  return saveConfig();
}

async function addAsset (asset) {
  let options = {
    basedir: this.basedir,
    config: path.join(this.basedir, 'config.json')
  }
  return new Promise((done) => {
    add.toConfig(this.config, options, {key: asset}, done);
  }, (err) => {done('error: ' + err)});
}

function server (options) {
  return new svr.connect(this.config, options);
}

async function status () {
  return new Promise((done) => {
    stat.check(null, this.config, {basedir: this.basedir}, done);
  });
}

async function exportAssets () {
  return new Promise((done) => {
    exp.prepare(this.config, {basedir: this.basedir}, done);
  });
}

async function packFiles () {
  return new Promise((done) => {
    pack.pack(this.config, {basedir: this.basedir}, done);
  });
}

module.exports = function (folder) {
  var config = init(folder);
  return {
    basedir,
    config,
    saveConfig,
    version,
    connected,
    setup,
    addTarget,
    addAsset,
    server,
    status,
    export: exportAssets,
    pack: packFiles
  }
}

