const assert = require('assert');
const fs = require('fs');
const https = require('https');
const server = require('./server.js');
if (!fs.existsSync('.temp')) {
  fs.mkdirSync('.temp');
}
const csfetch = require('../index.js')('.temp');
const PORT = 8989;

before(function () {
  server.run(PORT);
  if (!(csfetch.config.setup && csfetch.config.setup.server)) {
    csfetch.setup({
      username: 'csfetch',
      password: '',
      server: '127.0.0.1:' + PORT,
      service: '/ws/rest/service'
    });
  }
});

describe('Version', function () {
  it('should should return the current version', function () {
    let pack = require('../package.json');
    assert.equal(pack.version, csfetch.version);
  });
});

describe('Connected', function () {
  it('should be connected to server', function () {
    assert.doesNotReject(csfetch.connected());
  });
});

describe('Add target', function () {
  it('should add target', function () {
    assert.ok(csfetch.addTarget({
      key:'testing',
      server: '127.0.0.1:' + PORT,
      path: '.temp2',
      username:'csfetch',
      password:''}));
  });
});

describe('Add asset', function () {
  it('should add an asset to config.json and download it', async function () {
    await csfetch.addAsset(1000);
  });
});

describe('Show status', function () {
  it('should show status of downloaded assets', async function () {
    await csfetch.status();
  })
})

describe('Export assets', function () {
  it('export assets in config', async function () {
    await csfetch.export();
    assert.ok(fs.existsSync('./.temp/export/95d1f359-0485-468f-aa8e-a08202a13d03.asset.xml'));
  })
})

describe('Make package', function () {
  it('make a tar package', async function () {
    await csfetch.pack();
    let tarfile = 'package_' + (new Date().toISOString().substring(0, 10)) + '.tgz';
    assert.ok(fs.existsSync('./.temp/packages/' + tarfile));
  })
})

after(function () {
  setTimeout(() => {
    console.log('closing server ...');
  }, 100);
  setTimeout(function () {
    console.log('Deleting temp directory')
    fs.rmSync('.temp', {recursive: true});
    https.get(`https://127.0.0.1:${PORT}/exit`, { rejectUnauthorized: false }, (res) => {
      res.on('data', (data) => {
        process.stdout.write(data);
      });
    })
  }, 2500);
});



