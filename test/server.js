const https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('./test/key.pem'),
  cert: fs.readFileSync('./test/cert.pem')
};

function acceptAuth (req, res) {
  if (req.headers.authorization !== 'Basic Y3NmZXRjaDo=') {
    //console.log('NOT ACCEPT AUTH', req.headers);
    res.setHeader('Content-Type', 'application/xml');
    res.writeHead(403);
    res.end();
    return;
  }
  return true;
} 

let route = {
  '/ws/rest/service/assets/all;limit=0': (req, res) => {
    if (!acceptAuth(req, res)) return;
    res.setHeader('Content-Type', 'application/xml');
    res.writeHead(200);
    res.end('<?xml version="1.0" encoding="UTF-8"?><assets/>'); 
  },
  '/ws/rest/service/assets/asset/id/1000': (req, res) => {
    if (!acceptAuth(req, res)) return;
    res.setHeader('Content-Type', 'application/xml');
    res.writeHead(200);
    res.end('<?xml version="1.0" encoding="UTF-8"?><asset id="1000" version="1" type="text." name="dummy.txt">' +
      '<storage_item key="master" relpath="file:1/dummy.txt"/>' +
      '<asset_feature feature="censhare:uuid" value_string="95d1f359-0485-468f-aa8e-a08202a13d03"/>' +
      '<asset_feature feature="censhare:module-asset-source" value_string="./just_a_file.txt"/>' +
      '</asset>'); 
  /*  <storage_item sid="18591" asset_id="15943" asset_version="5" element_idx="0" key="master" filesys_name="assets" mimetype="application/xslt+xml" relpath="file:10/34/103420.xsl" state="0" original_path="/Users/steen/dev/cs-nodered/nodered-send-wf.xsl" original_name="nodered-send-wf.xsl" filelength="2186" modified_date="2020-11-28T14:38:13.237Z" rowid="7535" hashcode="5AC4319880FF1C7128E6AE5866A5E608F9CB13AD" url="censhare:///service/assets/asset/id/15943/version/5/element/actual/0/storage/master/file/103420.xsl"/> */
  },
  '/ws/rest/service/assets/asset/id/1000/storage/master/file': (req, res) => {
    if (!acceptAuth(req, res)) return;
    res.setHeader('Content-Type', 'text/plain');
    res.writeHead(200);
    res.end('This is a test!');
  },
  '/exit': (req, res) => {
    res.writeHead(200);
    res.end('bye');
    setTimeout(() => {
      process.exit();
    }, 500);
  }
}

function run (port) { 
  https.createServer(options, (req, res) => {
    if (route[req.url]) {
      route[req.url](req, res);
    } else {
      res.writeHead(200);
      res.end('');
    }
  }).listen(port);
}

module.exports = { run };
