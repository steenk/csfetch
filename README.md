# csfetch

This is a command to fetch source assets from a Censhare server, and keep track of changes.

# INSTALL

Prerequisite is node and npm. Install csfetch with npm:

```
npm i -g csfetch
```
On Windows there may be security configurations to consider. Running `Set-ExecutionPolicy unrestricted` in Powershell may help.

# USAGE

This will describe the use of the command `csfetch`. For using `csfetch` for scripting, look at [scripting_scfetch.md](scripting_csfetch.md).

Go to a clean folder and run the `csfetch` command. Since it need a "config.json" file, it will say so, but also deliver a XSL file to generate the config.json file. Take the the file "create-config.xsl" that comes out from the command and run it in a Censhare Java client, preferable in the "XSLT Evaluator Test" window. It will take all assets from the selected asset, down to every related asset, and create a JSON with a list of all these assets. Copy the JSON output to a file called "config.json" in the folder. The server and user account will already be in the config file, but if a password is needed for this account, it should be added under "/setup/password" in "config.json".

## SETUP

To setup a config file from scratch, run the command `csfetch setup` and you will get some guidence during the process. The resulting file "config.json" can be manually edited afterwards. Here is an example of a config file to start with.

```
{
 "setup": {
  "username": "censhare",
  "password": "",
  "server": "localhost:9443",
  "service": "/ws/rest/service",
  "mode": "classic",
  "prettyprint": true
 }
```

The "mode" keyword is used for undo an early change in the nameing of storage items. The asset-id from the dev server was added to the file name, and it turned out to be a bad idea. The "classic" mode was introduced to reverse this change without breaking it for some users.

The "prettyprint" is an optional keyword made for those who want to manually look into the asset files in the repository. It makes the XML be indented. Be aware that this option changes the XML, so it is no longer a cached copy of the original.

The "packpath" keyword is to change the pack path of the "pack" command. Default packages will be unpacked in the custom area when unpacked in the home directory on the application server.

## FETCH ASSETS

When a proper "config.json" is created, the command "csfetch" can be run again. It creates two folders, "assets" and "storage". Under "assets" are placed the XML of all assets in the config file, and for those assets with a master storage file, these are placed under "storage". These can now be kept in a safe location as a backup, or as a collection for deployment, to search in code files, or whatever.

There is a way do fetch chunks of assets for bad connections, by adding `"chunks": 10` in the setup section of the config file.

## ADD A SINGLE ASSET TO CONFIG

The command "csfetch add" downloads an asset from asset-id or resource key, and adds it to the config.json file. An optional subpath can can create subfolders in the storage area. Example.

```
$ csfetch add 44767 Freemarker
Written assets/ad93e5f0-5409-11e8-ae05-0a62a34f6c14.xml
Written storage/Freemarker/Hedin_Find_Cars.ftl
New entry for key 44767 added to config.json.
```

## CHECK STATUS 

To keep the assets download in sync, run the `csfetch` command again. There is also a command to see which assets have changed on the server, or still checked out. Run `csfetch status` and it will give a list of those assets changed on the server. If there is no output everything is in sync.

## ALTERNATIVE CONFIGS

Sometimes a smaller group of assets than included in the "config.json" are of interest. Then an alternative config file can be created and run with `csfetch config my-config-file.json`. Running from the same location as the "config.json", it will download and update files in the same folders. This is also a good way to expand the set of assets keeping track of, without download everything. To merge the alternative config into the main "config.json" without creating duplicates, run `csfetch merge my-config-file.json`.

## DOCUMENTATION

With the command `csfetch doc` a file "documentation.md" is created. Another name can be use with `csfetch doc my-own-name.md`. The result is a Markdown file with all assets sorted by asset type, and some properties describing these assets.

## EXPORT

In the config file activate an export by setting the export value to a subpath that will appear under "export". Then run `csfetch export`. An "export" folder will be created with prepared assets ready to import into a server.

## Pack

This is how to pack the export files into a tar file, with a structure made for the server. It has to run for a group, like `csfetch pack mygroup`.

## VERSION

Print the version of csfetch with `csfetch version`.

## SUMMARY

```
Usage:

csfetch [group]
csfetch add <asset-id or resource-key> [group]
csfetch merge config-json-file
csfetch config config-json-file
csfetch status
csfetch doc [my-own-file-name] [group]
csfetch version
csfetch export [group]
csfetch group [name] [export=xxx] [subpath=xxx]
csfetch setup
csfetch pack group
csfetch help
```

