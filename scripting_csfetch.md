# Scripting csfetch

There is an ongoing project to make `csfetch` a Nodejs module, so it can be used programmatically. Since it was not made for this from the beginning, there are some limitations that hopefully will be addressed over time.

This documentation uses EcmaScript module loading, instead of the old `require` loader previously used in Nodejs. Prepare your `package.json` with a `"type": "module"` to make it work. Then install csfetch in your project.

```sh
npm i csfetch
```

Import csfetch into your script.

```js
import CSFetch from 'csfetch';

const csfetch = CSFetch();
console.log(csfetch.version);
```

Optionally we can point to an existing folder, like `CSFetch('./repo')` when creating the instance. This makes it also possible to have several instances pointing to different directoryies and optionally config files.

## Setup

If there exists a `config.json` file in the running directory, or if at config file is provided in the parameter, its configuration is used. The connect configuration can also be passed to the `setup` method.

```js
csfetch.setup({
	username: 'censhare',
	password: 'secret',
	server: 'localhost:9443'
});
console.log(csfetch.config); // shows the content
csfetch.saveConfig(); // save the configuration
``` 

To make this setup create a 'config.json' file, set `true` as the second parameter to `setup`. You can anytime write the content into a `config.json` by calling the method `saveConfig()`.

## Fetch assets

Assets are fetched into the current "basedir" with the method `fetchAsset(asset_id)`.

```js
console.log('asset will be saved in', csfetch.basedir);
[18029, 18031, 18041].forEach(asset => {
        csfetch.addAsset(asset);
});
```

 